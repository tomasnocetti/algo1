import SoundPlayer as pysounds

class Cancion:

	def __init__(self):
		self.inicio()

	def load(self,archivo):
		with open(archivo, 'r') as audio:
			self.canales = int(audio.readline().rstrip('\n').split(',')[1])
			currentT = 0
			for linea in audio:
				linea=linea.rstrip('\n').split(',')
				if linea[0].lower() == 's':
					tipoOnda, frecuencia, volumen = linea[1].split('|')
					self.generarTrack(frecuencia, volumen, tipoOnda)      
				elif linea[0].lower() == 't':
					currentT = linea[1]
				elif linea[0].lower() == 'n':
					activos = []
					for i, x in enumerate(linea[1]):
						if x == '#':
							activos.append(i)
					tracks = []
					for x in activos:
						tracks.append(self.tracks[x])
					self.insertar_Marca(currentT, tracks, self.len)
					

	def insertar_Marca(self, tiempo, activos, posicion):
		marca = Marca(tiempo, activos)
		self.len += 1
		if posicion == 0:
			marca.prox = self.primMarca
			self.primMarca = marca
			# print (marca)
		else:
			actual = self.primMarca
			for x in range(1, posicion):
				actual = actual.prox
			marca.prox = actual.prox
			actual.prox = marca
			# print (marca)

	def append_Marca(self, tiempo, activo):
		self.insertar_Marca(tiempo, activos, self.len)

	# def sacar_Marca(self, posicion):
	#   if self.len == 0:
	#     raise ValueError("Cancion vacia")
	#   if  
	def inicio(self):
		self.canales = 0 #cantidad de canales que tiene la cancion
		self.tracks = [] #Todos los tracks de la cancion
		self.primMarca = None
		self.len = 0

	def generarTrack(self, frecuencia, volumen, tipoOnda):
		self.tracks.append(Track(frecuencia, volumen, tipoOnda))

	def generar_cancion(self):
		sp = pysounds.SoundPlayer(self.canales)
		actual = self.primMarca
		while actual.prox:
			sonido = []
			for x in actual.tracks:
				sonido.append(x._analizarSonido())
			sp.play_sounds(sonido, actual.tiempo)		
			actual = actual.prox
			

class Marca:
	def __init__(self, tiempo, activos):
		self.tiempo = float(tiempo) #Tiempo de duracion del track
		self.tracks = activos #Los que van a sonar en cada marca
		self.prox = None


	def generador_sonido(self,tiempo,canales):
		sp = pysounds.SoundPlayer(canales)
		sonido = []
		for x in self.tracks:
			sonido.append(x._analizarSonido())
		sp.play_sounds(sonido, tiempo)

	def __str__(self):
		return ('{}{}'.format(self.tiempo, self.tracks))

class Track:
	def __init__(self, frecuencia, volumen, tipoOnda):
		self.frecuencia = float(frecuencia)
		self.volumen = float(volumen)
		self.tipoOnda = tipoOnda
		self.dato = self._analizarSonido()

	def __str__(self):
		return 'frecuencia: {} volumen : {} onda: {}'.format (self.frecuencia, self.volumen, self.tipoOnda)

	def _analizarSonido(self):
		tipoOnda = self.tipoOnda.upper()
		generadorSonido = None
		if tipoOnda == 'SINE':
			generadorSonido = pysounds.SoundFactory.get_sine_sound
		elif tipoOnda == 'TRIA':
			generadorSonido = pysounds.SoundFactory.get_triangular_sound
		else:
			generadorSonido = pysounds.SoundFactory.get_square_sound
		return  generadorSonido(self.frecuencia, self.volumen)

can = Cancion()

can.load('Square.txt')

# print(can.canales)

can.generar_cancion()

# print(can.canales)

# actual = can.primMarca
# print (actual)
# for x in range (can.len-1):
#   actual = actual.prox
#   print (actual)