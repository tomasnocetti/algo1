#!/usr/bin/python3
# coding=utf-8
print("\n")
import cmd, cancion, helpers

class SCCShell(cmd.Cmd):
    intro = 'Bienvenidos a la terminal de SSC. Escriba help o ? para ver una lista de comandos.\n'
    prompt = '(SSC) '
    can = cancion.Cancion()

    def do_load(self, archivo):
        """LOAD <file> Carga la cancion desde el archivo. Reemplaza la cancion en edicion
        actual si es que la hay.
        Ejemplo :
        LOAD last surprise.plp
        Carga al modo de edicion la cancion “Last Surprise”. El cursor del
        editor queda al inicio de la misma. Si no existe el archivo, se le debe
        informar al usuario."""
        try:
          self.can.load(archivo)
          print("Archivo cargado exitosamente")
          print(self.can)
        except ValueError as err:
          print(err)

    def do_save(self, archivo):
        """STORE <file> Guarda la cancion.
        Ejemplo :
        STORE shadow world.plp
        Guarda la cancion en edicion actual con el nombre indicado."""

        try:
          if archivo == "":
            raise ValueError("Escriba un archivo valido")
          self.can.save(archivo)
          print("Archivo salvado exitosamente")
        except ValueError as err:
          print(err)

    def do_step(self, param):
        """STEP Avanza a la siguiente marca de tiempo.
        Ejemplo :
        STEP
        Eso avanza hacia la siguiente marca de tiempo. Si no hay mas marcas
        hacia adelante, no hace nada."""

        self.can.lista.mover_cursor()
        print(self.can)

    def do_stepm(self, param):
        """STEPM # Avanza N marcas de tiempo hacia adelante.
        Ejemplo :
        STEPM 1
        Eso avanza hacia la siguiente marca de tiempo. Si no hay mas marcas
        hacia adelante, no hace nada. Idem que STEP.
        Ejemplo :
        STEPM 6
        Eso avanza 6 marcas de tiempo hacia adelante. Si no hay suficientes
        marcas hacia adelante, se queda al final."""
        try:
          param = helpers.validar_positivo(param)

          self.can.lista.mover_cursor(param)
          print(self.can)
        except ValueError as error:
          print(error)

    def do_back(self, param):
        """BACK Retrocede a la anterior marca de tiempo.
        Ejemplo :
        BACK
        Eso retrocede hacia la anterior marca de tiempo. Si no hay mas marcas
        hacia atras, no hace nada."""

        self.can.lista.mover_cursor(1,True)
        print(self.can)

    def do_backm(self, param):
        """BACKM # Retrocede N marcas de tiempo hacia atras.
        Ejemplo :
        BACKM 1
        Eso retrocede hacia la anterior marca de tiempo. Si no hay mas marcas
        hacia atras, no hace nada. Idem que BACK
        Ejemplo :
        BACKM 5
        Eso retrocede 5 marcas de tiempo. Si no hay suficientes marcas hacia atras,
        se queda al inicio."""
        try:
          param = helpers.validar_positivo(param)
          self.can.lista.mover_cursor(param,True)
          print(self.can)
        except ValueError as err:
          print(err)

    def do_trackadd(self, params):
        """TRACKADD <funcion><frecuencia><volumen> Agrega un track con el sonido indicado.
        Ejemplo :
        Suponiendo que se tiene la funcion sine dentro de las que ofrece el
        programa:
        trackadd sine 440 0.2
        Eso agrega un track con un sonido generado por la funcion sine, a 440hz
        y con un volumen del 20 %."""

        try:
          function, frecuencia, volumen = params.upper().split()
          self.can.generar_track(frecuencia, volumen, function)
          self.can.canales += 1
          print(self.can)
        except ValueError as err:
          print(err)

    def do_trackdel(self, param):
        """TRACKDEL <track #> Elimina un track por numero.
        Ejemplo :
        Suponiendo que se tienen 3 tracks:
        TRACKDEL 2
        TRACKDEL 2
        Eso elimina primero el track en la posicion 2, y luego elimina el track
        que originalmente era el track 3, pero que luego del borrado ocupa la
        posicion 2."""
        try:
          self.can.track_accion(param, True, True)
          print(self.can)
        except ValueError as err:
          print(err)

    def do_tracklist(self, param):
        """TRACKLIST  Devuelve la lista de tracks registrados en la cancion."""

        for track in self.can.tracks:
          print(str(track) + " -> " + str(self.can.tracks[track]))

    def do_markadd(self, param):
        """MARKADD <duration> Agrega una marca de tiempo en la posicion actual del cursor de la duracion establecida. Originalmente
        todos los tracks arrancan como deshabilitados.
        Ejemplo :
        MARKADD 2
        Agrega una marca de tiempo de 2 segundos en la posicion actual del cursor."""
        try:
          self.can.marca_accion(param)
          print(self.can)
        except ValueError as err:
          print("El numero no es correcto")

    def do_list(self, param):
        """ Imprime una representacion grafica de la cancion con sus respectivas marcas y un cursor que indica
        en que posicion se esta """
        print(self.can)

    def do_markaddnext(self, param):
        """MARKADDNEXT <duration> Igual que MARKADD pero la inserta luego de la marca en la cual esta
        actualmente el cursor.
        """
        try:
          self.can.marca_accion(param, cancion.MARCA_ACCIONES["DESPUES"])
          print(self.can)
        except ValueError as err:
          print("El numero no es correcto")

    def do_markaddprev(self, param):
        """MARKADDPREV <duration> Igual que MARKADD pero la inserta antes de la marca en la cual esta
        actualmente el cursor.
        """
        try:
          self.can.marca_accion(param, cancion.MARCA_ACCIONES["ANTES"])
          print(self.can)
        except ValueError as err:
          print("El numero no es correcto")

    def do_trackon(self, param):
        """TRACKON <track #> Habilita al track durante la marca de tiempo en la cual esta parada el
        cursor.
        Ejemplo :
        Suponiendo que se tienen 3 tracks y la marca fue recien creada:
        TRACKON 2
        TRACKON 3
        Habilita los tracks 2 y 3 de la marca. Cuando se reproduzca la misma,
        se reproduciran los sonidos de los tracks 2 y 3 durante la duracion de
        la misma, pero no el del track 1.
        """
        try:
          self.can.track_accion(param)
          print(self.can)
        except ValueError as err:
          print(err)
        except AttributeError as err:
          print(err)

    def do_trackoff(self, param):
        """TRACKOFF <track #> Operacion inversa del TRACKON.
        Ejemplo :
        Suponiendo que se tiene 1 track:
        TRACKON 1
        TRACKOFF 1
        Cuando se reproduce esta marca, no se reproduce el sonido del track1."""
        try:
          self.can.track_accion(param, True)
          print(self.can)
        except ValueError as err:
          print(err)
        except AttributeError as err:
          print(err)

    def do_play(self, param):
        """PLAY Reproduce la marca en la que se encuentra el cursor actualmente.
        Ejemplo :
        PLAY"""
        try:
          self.can.play(cancion.CONFIG_REPRODUCCION["MARCAS"], "1")
          print(self.can)
        except AttributeError as err:
          print(err)

    def do_playall(self, param):
        """PLAYALL Reproduce la cancion completa desde el inicio.
        Ejemplo :
        PLAYALL"""
        try:
          self.can.play(cancion.CONFIG_REPRODUCCION["CANCION"])
          print(self.can)
        except AttributeError as err:
          print(err)

    def do_playmarks(self, param):
        """PLAYMARKS <#> Reproduce las proximas 5 marcas desde la posicion actual del cursor.
        Ejemplo :
        PLAYMARKS 5"""
        try:
          self.can.play(cancion.CONFIG_REPRODUCCION["MARCAS"], param)
          print(self.can)
        except ValueError as err:
          print(err)
        except AttributeError as err:
          print(err)

    def do_playseconds(self, param):
        """PLAYSECONDS <#> Reproduce los proximos 5 segundos la posicion actual del cursor. Si
        alguna marca dura mas del tiempo restante, la reproduccion se corta antes.
        Ejemplo :
        PLAYSECONDS 5"""
        try:
          self.can.play(cancion.CONFIG_REPRODUCCION["SEGUNDOS"], param)
          print(self.can)
        except ValueError as err:
          print("El numero no es correcto")
        except AttributeError as err:
          print(err)

    def do_exit(self, params):
        return True

if __name__ == '__main__':
    SCCShell().cmdloop()
