import pilas
print("\n")
class Iterador:
  def __init__(self, nodo):
    self.actual = nodo
    self.pila = pilas.Pila()
    self.anter = None

  def __len__(self):
    return self.len

  def anterior(self):
    if self.anter is None:
      raise StopIteration

    self.actual = self.anter
    if self.pila.esta_vacia():
      self.anter = None
    else:
      self.anter = self.pila.desapilar()
    return self.actual.dato

  def __next__(self):

    if self.actual is None:
      raise StopIteration()

    if not self.anter is None:
      self.pila.apilar(self.anter)

    self.anter = self.actual
    self.actual = self.actual.prox
    return self.anter.dato

class ListaEnlazada:
  ''' Clase que representa una lista enlazada simple con un iterador para movernos
  dentro de sus elementos '''

  def __init__(self):
    self.prim = None
    self.len = 0
    self.iter = None

  def __iter__(self):
    return Iterador(self.prim)

  def __str__(self):
    texto = ""

    for dato in self:
      cursor = "   "

      if dato == self.iter.actual.dato:
        cursor = ">  "
      texto += cursor + str(dato) + "\n"
    return texto

  def __len__(self):
    return self.len

  def insertar(self, dato, despues = False):
    '''Inserta un nodo a continuación del elemento actual del iterador a menos que se pase a la
    funcion la variable'''

    if self.prim == None:
      self.prim = Nodo(dato)
      self.iter = iter(self) #Inicio el iterador y guardo una referencia en la propia lista.
    elif(despues):
      self.iter.actual.prox = Nodo(dato, self.iter.actual.prox)
      next(self.iter)
    else:
      try:
        self.mover_cursor(1, True)
        self.iter.actual.prox = Nodo(dato, self.iter.actual.prox)
        self.mover_cursor(2)
      except StopIteration:
        # En caso de que levante la exepcion significa que ya se encuentra al principio entonces
        # el nodo sera el primero. Debo reinstanciar el iterador en esta situacion.
        actual = self.prim
        self.prim = Nodo(dato, actual)
        self.iter = Iterador(self.prim)
        self.mover_cursor(1)

    self.len += 1

  def mover_cursor(self, pasos = 1, atras = False):
    '''Funcion que se encarga de mover el cursor del iterador desde afuera de la clase ListaEnlazada
    Recibe como parametro los pasos y opcionalmente si la direccion es para atras.'''

    try:
      for x in range(pasos):
        if atras:
          self.iter.anterior()
        else:
          if not self.iter.actual.prox is None:
            next(self.iter)
      return
    except StopIteration:
      return

  def devolver_actual(self):
    '''Funcion que devuelve el dato del nodo actual de la lista'''

    return self.iter.actual.dato

  def generar_iterador_actual(self):
    '''Funcion que devuelve un iterador generado a partir del nodo actual de la lista'''

    return Iterador(self.iter.actual)

class Nodo:
  '''Clase que representa el nodo de la lista enlazada'''

  def __init__(self, dato, prox = None):
    self.dato = dato
    self.prox = prox

  def __str__(self):
    return self.dato
