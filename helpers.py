import os

def pedirNumero(mensaje):
  """Le pide al usuario que ingrese lo indicado por parametro. Devuelve el resultado
  previamente de haberlo casteado a una variable del tipo int."""

  while True:
    answ = input(mensaje)
    if answ.isnumeric():
      return int(answ)

def validar_positivo(num):
  """ Recibe cadena y valida que sea un numero y positivo. Devuelve numero en formato int. Levanta error en caso
  de error """

  if not num.isdigit():
    raise ValueError("El valor no es un numero positivo")
  return int(num)

def obtener_directorio(archivo):
  """ Valida que el archivo exista. Recibe el nombre del archivo. Devuelve la ruta o en caso de que no lo encuentre un
  mensaje de error"""
  ruta = os.path.dirname(__file__)
  rutaArchivo = os.path.join(ruta, archivo)

  if(os.path.isfile(rutaArchivo)):
    return rutaArchivo
  else:
    return None
