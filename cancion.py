##!/usr/bin/python3

import SoundPlayer as pysounds
import helpers, lista

CONFIG_ARCHIVO = {
  "CANAL": "C",
  "TRACK": "S",
  "TIEMPO": "T",
  "MARCA": "N",
  "SEPARADOR": ",",
  "SIMBOLO_TRACK": "#",
  "DATOS_POR_LINEA": 2
}

CONFIG_REPRODUCCION = {
  "CANCION": 1,
  "SEGUNDOS": 2,
  "MARCAS": 3
}

MARCA_ACCIONES = {
  "ANTES": 1,
  "DESPUES": 2
}

class Cancion:
  ''' Esta clase representa a la cancion que el usuario estara modificando. Dentro de ella
  se almacenara una lista con las marcas de la cancion, un diccionario con los tracks y la
  cantidad de canales. Tambien contendra las funciones para moverse, guardar, cargar y editar
  la cancion'''

  def __init__(self):
    self.canales = 0 #cantidad de canales que tiene la cancion
    self.tracks = {} #Diccionario con todos los tracks de la cancion
    self.lista = lista.ListaEnlazada()

  def __len__(self):
    return self.len

  def __str__(self):
    return str(self.lista)

  def load(self,archivo):
    """Funcion que carga el archivo, genera los nodos y los tracks llamando a sus reespectivas funciones
    Pre: recibe el nombre del archivo con su extension
    Post: Genera Marcas, Tracks y La lista enlazada"""

    # Valido que el archivo exista
    if(helpers.obtener_directorio(archivo) is None):
      raise ValueError("El archivo especificado no se encuentra")
    # Ya que estoy cargando una nueva cancion reinicio la instancia
    self.__init__()
    with open(archivo, 'r') as audio:
      currentT = 0
      tracks = []
      # Recorro todas las lineas del archivo
      for i, currLinea in enumerate(audio):
        # Elimino los espacios y genero una lista con la informacion a partir del separador
        linea = currLinea.rstrip('\n').split(CONFIG_ARCHIVO["SEPARADOR"])
        # Valido que la linea tenga la informacion correspondiente
        if len(linea) != CONFIG_ARCHIVO["DATOS_POR_LINEA"]:
          raise ValueError("El formato del archivo es incorrecto. Linea({}): {}".format(i+1, currLinea))
        letra, dato = linea
        if i == 0 and letra.lower() == CONFIG_ARCHIVO["CANAL"]:
          # Si es la primera linea y letra (currentLinea[0]) es igual a la constante del canal
          if not dato.isnumeric():
            raise ValueError("El formato del archivo es incorrecto. Linea({}): {}".format(i+1, currLinea))
          # Seteo los canales
          self.canales = int(dato)
        elif letra.lower() == CONFIG_ARCHIVO["TRACK"]:
          # Si la letra (currentLinea[0]) es igual a la constante del track
          tipoOnda, frecuencia, volumen = dato.split('|')
          # Genero un track de la cancion y guardo la respuesta en una lista local para luego ver cual de cada
          # track se encuentra activado o no.
          tracks.append(self.generar_track(frecuencia, volumen, tipoOnda))
        elif letra.lower() == CONFIG_ARCHIVO["TIEMPO"]:
          # Si la letra (currentLinea[0]) es igual a la constante de tiempo
          try:
            # Seteo currentT
            currentT = float(dato)
          except ValueError:
            raise ValueError("El formato del archivo es incorrecto. Linea({}): {}".format(i+1, currLinea))
        elif letra.lower() == CONFIG_ARCHIVO["MARCA"]:
          # Si la letra (currentLinea[0]) es igual a la constante de marca
          activos = []
          if len(dato) != len(self.tracks):
            # Valido que el largo del dato (...#...) sea igual a la cantidad de canales
            raise ValueError("El formato del archivo es incorrecto. Linea({}): {}".format(i+1, currLinea))
          for i, x in enumerate(dato):
            # Por cada item del dato configuro los tracks activos
            if x == CONFIG_ARCHIVO["SIMBOLO_TRACK"]:
              activos.append(int(tracks[i]))
          # Creo la marca y la inserto en la lista
          self.lista.insertar(Marca(currentT, activos))
        else:
          raise ValueError("El formato del archivo es incorrecto. Linea({}): {}".format(i+1, currLinea))

  def save(self, archivo):
    """Funcion que se encarga de transcribir la cancion al formato '.plp'.
    Pre: recolecta los datos de la cancion
    Post: Genera el archivo '.plp'"""

    with open(archivo, 'w') as audio:
      # Escribo en la linea la letra configurada en para la global del canal y su valor
      audio.write(CONFIG_ARCHIVO["CANAL"] + "," + str(self.canales) + "\n")
      tracks = []
      for track in self.tracks:
        # Por cada track en la cancion escribo la letra configurada en para la global del track y su valor
        tracks.append(track)
        audio.write(CONFIG_ARCHIVO["TRACK"] + "," + str(self.tracks[track]) + "\n")
      currentT = None
      for i, marca in enumerate(self.lista):
        # Por cada marca en la cancion escribo la letra configurada en para la global de marca y tiempo y su valor
        # cuando es necesario
        if marca.tiempo != currentT:
          currentT = marca.tiempo
          audio.write(CONFIG_ARCHIVO["TIEMPO"] + "," + str(currentT) + "\n")
        audio.write(CONFIG_ARCHIVO["MARCA"] + ",")
        for track in self.tracks:
          if track in marca.tracks:
            audio.write("#")
          else:
            audio.write(".")
        if i != len(self.lista) -1:
          audio.write("\n")

  def marca_accion(self, param, accion = None):
    """ En este metodo se combinan las posibilidades de inserccion de una marca en la lista de la cancionº.
    Dependiendo del parametro insertara la marca en un lugar correspondiente."""

    marca = Marca(float(param))
    if accion == MARCA_ACCIONES["ANTES"]:
      self.lista.insertar(marca)
    elif accion == MARCA_ACCIONES["DESPUES"]:
      self.lista.insertar(marca, True)
      self.lista.mover_cursor(1, True)
    else:
      self.lista.insertar(marca)
      self.lista.mover_cursor(1, True)

  def generar_track(self, frecuencia, volumen, tipoOnda):
    """Genera un track con los parametros
    Pre: recibe Frecuencia, Volumen y Tipo de Onda.
    Post: Lo agrega al diccionario de tracks y devuelve su clave"""

    volumen = float(volumen)
    frecuencia = float(frecuencia)
    if "SQ" in tipoOnda:
      tipoOnda = "SQUA"
    for trackId in self.tracks:
      # Si ya tengo el track que pide el usuario le devuelvo el ID.
      if str(self.tracks[trackId]) == '{}|{}|{}'.format(tipoOnda, frecuencia, volumen):
        return trackId
    track = Track(frecuencia, volumen, tipoOnda)
    self.tracks[track.id] = track
    return track.id

  def play(self, tipoReproduccion, cantidad = 0):
    """ En este metodo se combinan las posibilidades de reproduccion de una marca en una lista.
    Dependiendo del parametro definido en la variable global CONFIG_REPRODUCCION se elegira el
    tipo de reproduccion."""

    if not len(self.lista) > 0:
      raise AttributeError("No existen marcas en la cancion")
    marcaActual = self.lista.devolver_actual()
    #Creo un nuevo iterador para moverme y no utilizar el del cursor
    nuevoIterador = self.lista.generar_iterador_actual()
    #Inicio el objeto de reproduccion
    sp = pysounds.SoundPlayer(self.canales)
    if tipoReproduccion == CONFIG_REPRODUCCION["CANCION"]:
      # Si la reproduccion es toda la cancion
      for marca in self.lista:
        sp.play_sounds(marca.obtener_sonidos(self.tracks), marca.tiempo)
    elif tipoReproduccion == CONFIG_REPRODUCCION["SEGUNDOS"]:
      # Si la reproduccion es una cantidad de segundos
      tiempoT = float(cantidad)
      try:
        while True:
          marcaActual = next(nuevoIterador)
          if marcaActual.tiempo < tiempoT:
            sp.play_sounds(marcaActual.obtener_sonidos(self.tracks), marcaActual.tiempo)
          else:
            sp.play_sounds(marcaActual.obtener_sonidos(self.tracks), tiempoT)
            break
          tiempoT -= marcaActual.tiempo
      except StopIteration:
        pass
    elif tipoReproduccion == CONFIG_REPRODUCCION["MARCAS"]:
      # Si la reproduccion es por marcas
      cantidad = helpers.validar_positivo(cantidad)
      try:
        for x in range(cantidad):
          marcaActual = next(nuevoIterador)
          sp.play_sounds(marcaActual.obtener_sonidos(self.tracks), marcaActual.tiempo)
      except StopIteration:
        pass
    #Cierro el objeto de sonido
    sp.close()

  def track_accion(self, track, eliminar = False, todos = False):
    """Funcion que desactiva o activa un track de la marca actual. Si el parametro eliminar es Verdadero elimina
    el track del diccionario y de todas las marcas. Si el parametro eliminar y el parametro todos son positivos se
    eliminara al track de todas las marcas."""

    track = helpers.validar_positivo(track)
    marcaActual = self.lista.devolver_actual()
    if track not in self.tracks:
      raise ValueError("El numero de track es incorrecto o no existe")
    if todos:
      if eliminar:
        del self.tracks[track]
        self.canales -= 1
        for marca in self.lista:
          marca.eliminar_track(track)
    else:
      if eliminar:
        marcaActual.eliminar_track(track)
      else:
        marcaActual.agregar_track(track)

class Marca:
  """ Clase Marca, son los nodos de la lista enlazada, las que 'contienen' los sonidos"""

  def __init__(self, tiempo, activos = []):
    self.tiempo = float(tiempo) #Tiempo de duracion del track
    self.tracks = activos[:] #Los que van a sonar en cada marca

  def __str__(self):
    return '{} - {}'.format(self.tiempo, self.tracks)

  def eliminar_track(self, track):
    """ Elimina el ID del track del objeto Marca. No elimina el track del diccionario de tracks
    de la cancion"""
    if track in self.tracks:
      self.tracks.remove(track)

  def agregar_track(self, track):
    """ Agrega el ID del track del objeto Marca. No agrega el track del diccionario de tracks
    de la cancion"""
    if track not in self.tracks:
      self.tracks.append(track)

  def obtener_sonidos(self, tracks):
    """Funcion que no recibe parametros y devuelve una lista con los objetos sonidos guardados
    en el diccionario de tracks"""
    sonido = []
    for track in self.tracks:
      sonido.append(tracks[track].dato)
    return sonido

class Track:
  """Clase Track encargada de contener la instancia del objeto de la libreria pysound.
  Mantiene la información de frecuencia, volumen y tipoOnda"""
  tId = 0

  def __init__(self, frecuencia, volumen, tipoOnda):
    """ Inicializa el track, se le asigna un Id unico a la instancia para su manejo.
    Recibe parametros frecuencia(float), volumen(float), tipoOnda(sine/squa/tria) """
    Track.tId += 1

    self.id = Track.tId
    self.frecuencia = float(frecuencia)
    self.volumen = float(volumen)
    self.tipoOnda = tipoOnda
    self.dato = self._analizar_sonido()

  def __str__(self):
    return '{}|{}|{}'.format(self.tipoOnda, self.frecuencia, self.volumen)

  def _analizar_sonido(self):
    """ Funcion privada que genera el objeto de la libreria pysounds. No recibe parametros, no
    devuelve nada. """

    tipoOnda = self.tipoOnda.upper()
    generador_sonido = None
    if tipoOnda == 'SINE':
      generador_sonido = pysounds.SoundFactory.get_sine_sound
    elif tipoOnda == 'TRIA':
      generador_sonido = pysounds.SoundFactory.get_triangular_sound
    elif tipoOnda == 'SQUA':
      generador_sonido = pysounds.SoundFactory.get_square_sound
    else:
      raise ValueError("El tipo de onda no se reconoce")
    return  generador_sonido(self.frecuencia, self.volumen)
